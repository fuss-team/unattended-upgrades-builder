# Unattended upgrades builder

You can pass a `VERSION` parameter to the pipeline that defaults to `1.17`
to build a specific version of unattended.

Currently, 1.17 is the latest version compatible with Debian Buster since in 1.18
dependency on a greater version of `python3-apt` has been added.